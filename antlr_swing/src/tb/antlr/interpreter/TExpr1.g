tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    
  : (e1=expr
  | declare
  | print)*
  ;

declare 
  : ^(VAR id=ID) {declareSymbol($id.text);}
  ;
  
print
  : ^(PRINT e1=expr) {print($e1.out);}
  ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(POW   e1=expr e2=expr) {$out = power($e1.out, $e2.out);}
        | ^(PODST id=ID   e2=expr) {$out = setSymbol($id.text, $e2.out);}
        | INT                      {$out = getInt($INT.text);}
        | ID                       {$out = getSymbol($ID.text);}
        ;
