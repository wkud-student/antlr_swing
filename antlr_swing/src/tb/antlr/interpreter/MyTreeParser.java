package tb.antlr.interpreter;
import java.lang.Math;
import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.*;

public class MyTreeParser extends TreeParser {

	protected GlobalSymbols symbols = new GlobalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void print(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }
    protected void print(Integer value) {
        print(value.toString());
    }
    
    protected int power(int number, int exponent) {
    	return (int) Math.pow(number, exponent);
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected void declareSymbol(String name) {
		symbols.newSymbol(name);
	}
	
	protected Integer setSymbol(String name, Integer value) {
		symbols.setSymbol(name, value);
		return value;
	}
	
	protected Integer getSymbol(String name) {
		return symbols.getSymbol(name);
	}
}
